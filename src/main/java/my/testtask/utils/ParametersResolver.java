package my.testtask.utils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.user.ApplicationUser;

public class ParametersResolver {

    public static void checkEmailAddress(String address){
        // TODO: Complete email checking logic
        if(address == null) throw new NullPointerException("EMail address is Null!");
    }

    public static Status getStatus(String statusName) throws NotFoundException{
        Status status = ComponentAccessor.getConstantsManager().getStatusByNameIgnoreCase(statusName);
        if(status == null) throw new NotFoundException("Status with name: '" + statusName + "' not found!");
        else return status;
    }

    public static ApplicationUser getUser(String name) throws NotFoundException{
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByName(name);
        if(user == null) throw new NotFoundException("User with name: '" + name + "' not found!");
        return user;
    }

    private ParametersResolver(){}
}
