package my.testtask.service;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
import my.testtask.utils.ParametersResolver;

public class JiraMailDeliveryService extends AbstractService {
    private String address = "";
    private String statusName = "";
    private String userName = "";

    public void run() {
        ParametersResolver.checkEmailAddress(address);
        ApplicationUser user = ParametersResolver.getUser(userName);
        Status status = ParametersResolver.getStatus(statusName);
        IssuesWhereStatusSender sender = new IssuesWhereStatusSender(address, user, status);
        sender.send();
    }

    @Override
    public void init(PropertySet props) throws ObjectConfigurationException {
        address = props.getString("Email");
        statusName = props.getString("Status");
        userName = props.getString("Username");
        super.init(props);
    }

    public ObjectConfiguration getObjectConfiguration() throws ObjectConfigurationException {
        return getObjectConfiguration("jiramaildeliveryservice", "configuration.xml", null);
    }
}
