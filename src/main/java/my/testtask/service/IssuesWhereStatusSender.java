package my.testtask.service;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.export.customfield.DefaultCsvIssueExporter;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.views.csv.SearchRequestCsvViewAllFields;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.mail.builder.EmailBuilder;
import com.atlassian.jira.mail.util.ByteArrayDataSource;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParamsImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.component.TableLayoutFactory;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.query.Query;
import com.atlassian.query.order.SortOrder;
import org.joda.time.DateTime;

import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.io.*;
import java.util.Map;

public class IssuesWhereStatusSender {

    private static final String SUBJECT_TEMPLATE = "CSV Files from JiraMailDeliveryService";
    private static final String BODY_TEMPLATE = "This is mail from Jira Mail Delivery Service.";
    private String emailAddress;
    private Status status;
    private ApplicationUser user;
    private SearchRequestCsvViewAllFields csvFields;

    public IssuesWhereStatusSender(String emailAddress, ApplicationUser user, Status status) {
        this.emailAddress = emailAddress;
        this.status = status;
        this.user = user;

        this.csvFields = new SearchRequestCsvViewAllFields(
                ComponentAccessor.getApplicationProperties(),
                ComponentAccessor.getComponentOfType(DefaultCsvIssueExporter.class),
                ComponentAccessor.getJiraAuthenticationContext(),
                ComponentAccessor.getComponentOfType(TableLayoutFactory.class));

        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        authenticationContext.setLoggedInUser(user);
    }

    public void send() {
        SearchRequest request = new SearchRequest(makeQuery());
        SearchRequestParamsImpl requestParams = new SearchRequestParamsImpl((Map) null, new PagerFilter());
        EmailBuilder emailBuilder;
        CharArrayWriter charArrayWriter = new CharArrayWriter();
        try {
            csvFields.writeSearchResults(request, requestParams, charArrayWriter);
            emailBuilder = buildEmail(charArrayWriter);
            MailQueue mailQueue = ComponentAccessor.getMailQueue();
            MailQueueItem item = emailBuilder.renderLater();
            mailQueue.addItem(item);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            charArrayWriter.close();
        }
    }

    private EmailBuilder buildEmail(CharArrayWriter charArrayWriter) {
        EmailBuilder emailBuilder = new EmailBuilder(new Email(emailAddress)
                , "text/csv"
                , ComponentAccessor.getLocaleManager().getLocaleFor(user));
        ByteArrayInputStream inputStream = new ByteArrayInputStream(new String(charArrayWriter.toCharArray()).getBytes());
        try {
            ByteArrayDataSource dataSource = new ByteArrayDataSource(inputStream, "text/csv");
            DataHandler dataHandler = new DataHandler(dataSource);
            MimeBodyPart attache = new MimeBodyPart();
            attache.setDataHandler(dataHandler);
            DateTime dateTime = new DateTime();
            attache.setFileName(dateTime.toString() + "-jira.csv");
            emailBuilder = emailBuilder.addAttachment(attache).withBody(BODY_TEMPLATE).withSubject(SUBJECT_TEMPLATE);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return emailBuilder;
    }

    private Query makeQuery() {
        JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
        queryBuilder.where().status().eq(status.getName());
        queryBuilder.orderBy().issueId(SortOrder.DESC);
        return queryBuilder.buildQuery();
    }
}
